/**
 * An enumeration of NetSuite Account IDs
 *
 * @enum {string} NetSuiteAccounts
 * @readonly
 *
 * @license MIT
 * @copyright 2022 Stoic Software
 * @author Eric T Grubaugh <eric@stoic.software>
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define([], () => ({
  PRODUCTION: 'TSTDRV1555136',
  RELEASE_PREVIEW: '',
  SANDBOX1: '',
  SANDBOX2: ''
}));
