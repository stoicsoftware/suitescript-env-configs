/**
 * Contrived configuration values for the Sandbox 1 environment
 *
 * @enum {SomeConfig}
 * @readonly
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define([], () => ({
  SummaryEmail: {
    Recipient: 'devs@yourcompany.com',
    Sender: -5,
    TemplateFile: 998899
  },
  IsTestingMode: true
}));
