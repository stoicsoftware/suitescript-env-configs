/**
 * Contrived configuration values for the Production environment
 *
 * @enum {SomeConfig}
 * @readonly
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define([], () => ({
  SummaryEmail: {
    Recipient: 'accounting@yourcompany.com',
    Sender: 123,
    TemplateFile: 11223344
  },
  IsTestingMode: false
}));
