/**
 * Configuration values for the SomeConfig application
 *
 * @typedef {Object} SomeConfig
 *
 * @property {boolean} IsTestingMode true to enable Testing Mode for this application; false
 *  otherwise
 * @property {Object} SummaryEmail Configuration values for the summary email sent by the SomeConfig
 *  application
 * @property {string} SummaryEmail.Recipient Email address of the summary email primary recipient
 * @property {number} SummaryEmail.Sender Internal ID of the Employee used as the Sender of the
 *  summary email
 * @property {number} SummaryEmail.TemplateFile Internal ID of the File used as the template for the
 *  summary email
 */
