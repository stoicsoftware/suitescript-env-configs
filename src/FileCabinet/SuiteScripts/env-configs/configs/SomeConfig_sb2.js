/**
 * Contrived configuration values for the Sandbox 2 environment
 *
 * @enum {SomeConfig}
 * @readonly
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define([], () => ({
  SummaryEmail: {
    Recipient: 'qa@yourcompany.com',
    Sender: 444,
    TemplateFile: 87654321
  },
  IsTestingMode: true
}));
