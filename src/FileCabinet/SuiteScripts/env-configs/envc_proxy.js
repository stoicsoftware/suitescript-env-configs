define(
  ['SuiteScripts/enums/NetSuiteAccounts', 'N/runtime'],
  (NetSuiteAccounts, runtime) => {
  /**
   * Proxy interface for accessing environment-specific configuration values
   *
   * @type {Object} envc-proxy
   *
   * @copyright 2022 Stoic Software
   * @author Eric T Grubaugh <eric@stoic.software>
   *
   * @NApiVersion 2.1
   * @NModuleScope Public
   */
  let exports = {};

  return exports;
});
