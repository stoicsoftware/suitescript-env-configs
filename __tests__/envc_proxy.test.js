import proxy from '../src/FileCabinet/SuiteScripts/env-configs/envc_proxy';

global.NetSuiteAccounts = jest.requireActual('../src/FileCabinet/SuiteScripts/enums/NetSuiteAccounts');

describe('envc_proxy', () => {
  it('retrieves the value based on the environment', () => {
    console.log(NetSuiteAccounts.PRODUCTION);
    expect(true).toBeFalsy();
  });
  it('retrieves the value on the first attempt', () => {
    expect(true).toBeFalsy();
  });
  it('retrieves the memoized value on subsequent attempts', () => {
    expect(true).toBeFalsy();
  });
});
